import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Post } from '../../models/post';
import * as PostActions from '../../actions/post';

interface AppState {
    post: Post;
}

@Component({
    selector: 'app-post-object',
    templateUrl: './post-object.html',
    styleUrls: ['./post-object.scss']
})
export class PostObjectComponent implements OnInit {

    post: Observable<Post>;
    text: string;

    constructor(private _router: Router, private store: Store<AppState>) {
        this.post = this.store.select('post')
    }

    ngOnInit() {
    }

    goNext() {
        this._router.navigate(['/redux/counter']);
    }

    editText() {
        this.store.dispatch(new PostActions.EditText(this.text));
    }
    resetPost() {
        this.store.dispatch(new PostActions.Reset());
    }
    upvote() {
        this.store.dispatch(new PostActions.Upvote());
    }
    downvote() {
        this.store.dispatch(new PostActions.Downvote());
    }
}
