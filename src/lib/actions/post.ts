import { Action } from '@ngrx/store';

export const POST_EDIT_TEXT = '[Post] Edit';
export const POST_UPVOTE = '[Post] Upvote';
export const POST_DOWNVOTE = '[Post] Downvote';
export const POST_RESET = '[Post] Reset';

export class EditText implements Action {
    readonly type = POST_EDIT_TEXT;

    // Use a constuctor to send a payload with the action
    constructor(public payload: string) { }
}

export class Upvote implements Action {
    readonly type = POST_UPVOTE;
}

export class Downvote implements Action {
    readonly type = POST_DOWNVOTE;
}

export class Reset implements Action {
    readonly type = POST_RESET;
}

export type All = Upvote | Downvote | Reset | EditText;
