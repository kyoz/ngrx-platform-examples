/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SampleServiceService } from './sample-service.service';

describe('SampleServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SampleServiceService]
    });
  });

  it('should ...', inject([SampleServiceService], (service: SampleServiceService) => {
    expect(service).toBeTruthy();
  }));
});
