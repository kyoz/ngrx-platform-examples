import {
  LANGUAGE_SPANISH,
  LANGUAGE_FRENCH,
  LANGUAGE_ENGLISH
} from './../../reducers';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

interface AppState {
  language: string;
}

@Component({
  selector: 'app-language-switcher',
  templateUrl: './language-switcher.html',
  styleUrls: ['./language-switcher.scss']
})
export class LanguageSwitcherComponent implements OnInit {

  language: Observable<string>;

  constructor(private _router: Router, private store: Store<AppState>) {
    this.language = store.select('language');
  }

  ngOnInit() {
  }

  goNext() {
    this._router.navigate(['/redux/post']);
  }

  message(language) {
    if (language === 'french') {
      this.store.dispatch({ type: LANGUAGE_FRENCH });
    } else if (language === 'spanish') {
      this.store.dispatch({ type: LANGUAGE_SPANISH });
    } else {
      this.store.dispatch({ type: LANGUAGE_ENGLISH });
    }
  }
}
