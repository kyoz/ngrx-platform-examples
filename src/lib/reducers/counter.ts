import { Action } from '@ngrx/store';

export const COUNTER_INCREMENT = 'INCREMENT';
export const COUNTER_DECREMENT = 'DECREMENT';
export const COUNTER_RESET = 'RESET';

export function counterReducer(state: number = 0, action: Action) {
  switch (action.type) {
    case COUNTER_INCREMENT:
      return state + 1;
    case COUNTER_DECREMENT:
      return state - 1;
    case COUNTER_RESET:
      return 0;
    default:
      return state;
  }
}
